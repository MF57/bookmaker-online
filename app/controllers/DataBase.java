package controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class DataBase {

	static Logger logger = Logger.getLogger(DataBase.class);
	private Properties properties;
	private Connection conn;
	private PreparedStatement pst = null;
	private String url, username, password;
	private ResultSet rs;

	public DataBase() {
		conn = null;
		rs = null;
		pst = null;
		properties = new Properties();
		url = new String("jdbc:mysql://mysql.agh.edu.pl:3306/pbochene");
		username = new String("pbochene");
		password = new String("GH6AvkT3");
	}

	public boolean connectToDataBase() {
		this.properties.put("user", this.username);
		this.properties.put("password", this.password);
		this.properties.put("url", this.url);

		try {
			this.conn = DriverManager.getConnection(
					properties.getProperty("url"), properties);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error("SQLERROR", e);
			return false;
		}
	}

	public boolean disconnectFromDataBase() {
		try {
			this.conn.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error("SQLERROR", e);
			return false;
		}
	}

	public boolean insertIntoDataBase(String query) {

		try {
			this.pst = this.conn.prepareStatement(query);
			this.pst.executeUpdate();
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error("SQLERROR", e);
			return false;
		}
	}

	public ResultSet retrieveFromDataBase(String query) {

		try {
			this.pst = this.conn.prepareStatement(query);
			rs = pst.executeQuery();
			if (rs.next() == false)
				rs = null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			rs = null;
			logger.error("SQLERROR", e);
		}
		return rs;
	}

	public boolean updateMatch(int id, String[] properties) {
		String query = "UPDATE `pbochene`.`MATCHES` SET STATUS='";
		query += properties[0];
		query += "', TIME='";
		if (properties[1].endsWith("'")) {
			query += properties[1].substring(0, properties[1].length() - 1);
			query += "min";
		} else {
			query += properties[1];
		}
		query += "', SCORE1='";
		query += properties[2];
		query += "', SCORE2='";
		query += properties[3];
		query += "', WHOWON='";
		query += properties[4];
		query += "' WHERE id = '";
		query += id;
		query += "'";

		try {
			this.pst = this.conn.prepareStatement(query);
			this.pst.executeUpdate();
			query = "UPDATE `pbochene`.`BOOKEDMATCHES` SET RESULT='";
			query += properties[4];
			query += "' WHERE MATCHID = '";
			query += id;
			query += "'";
			this.pst = this.conn.prepareStatement(query);
			this.pst.executeUpdate();
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error("SQLERROR", e);
			return false;
		}

	}

}
