package controllers;

import java.util.ArrayList;
import java.util.List;

import models.UserInfoDB;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.Index;
import views.html.Profile;
import views.html.Login;
import views.formdata.LoginFormData;
import views.formdata.RegisterFormData;
import views.formdata.BookMatchFormData;
import play.mvc.Security;

/**
 * Implements the controllers for this application.
 */
public class Application extends Controller {

	/**
	 * Provides the Index page.
	 * 
	 * @return The Index page.
	 */
	public static Result index() {
		return ok(Index.render("Home", Secured.isLoggedIn(ctx()),
				Secured.getUserInfo(ctx())));
	}

	public static Result register() {
		Form<RegisterFormData> formData = Form.form(RegisterFormData.class);
		return ok(views.html.Register
				.render("Register", Secured.isLoggedIn(ctx()),
						Secured.getUserInfo(ctx()), formData));
	}

	public static Result test(String text) {
		return ok(views.html.Test.render(text));
	}

	/**
	 * Provides the Login page (only to unauthenticated users).
	 * 
	 * @return The Login page.
	 */
	public static Result login() {
		Form<LoginFormData> formData = Form.form(LoginFormData.class);
		return ok(Login.render("Login", Secured.isLoggedIn(ctx()),
				Secured.getUserInfo(ctx()), formData));
	}

	/**
	 * Processes a login form submission from an unauthenticated user. First we
	 * bind the HTTP POST data to an instance of LoginFormData. The binding
	 * process will invoke the LoginFormData.validate() method. If errors are
	 * found, re-render the page, displaying the error data. If errors not
	 * found, render the page with the good data.
	 * 
	 * @return The index page with the results of validation.
	 */
	public static Result postLogin() {

		// Get the submitted form data from the request object, and run
		// validation.
		Form<LoginFormData> formData = Form.form(LoginFormData.class)
				.bindFromRequest();

		if (formData.hasErrors()) {
			flash("error", "Login credentials not valid.");
			return badRequest(Login.render("Login", Secured.isLoggedIn(ctx()),
					Secured.getUserInfo(ctx()), formData));
		} else {
			// email/password OK, so now we set the session variable and only go
			// to authenticated pages.
			session().clear();
			session("email", formData.get().email);
			return redirect(routes.Application.profile());
		}
	}

	public static Result postRegister() {

		
		Form<RegisterFormData> formData = Form.form(RegisterFormData.class)
				.bindFromRequest();
		if (formData.field("password").value()
				.equals(formData.field("conpassword").value()) == false
				|| formData.field("email").value().isEmpty()
				|| formData.field("name").value().isEmpty()
				|| formData.field("password").value().isEmpty()
				|| formData.field("conpassword").value().isEmpty()) {
			formData.reject("");
		}
		String query = "SELECT * FROM `pbochene`.`USERS` WHERE MAIL='";
		query += formData.field("email").value();
		query += "'";
		if (!UserInfoDB.registrationApproved(query))
			formData.reject("");
		if (formData.hasErrors()) {
			flash("error", "Register credentials not valid.");
			return badRequest(views.html.Register.render("Register",
					Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx()),
					formData));
		} else {
			session().clear();
			UserInfoDB
					.addUserInfo(formData.field("name").value(), formData
							.field("email").value(), formData.field("password")
							.value());
			return redirect(routes.Application.register());
		}
	}

	/**
	 * Logs out (only for authenticated users) and returns them to the Index
	 * page.
	 * 
	 * @return A redirect to the Index page.
	 */
	@Security.Authenticated(Secured.class)
	public static Result logout() {
		session().clear();
		return redirect(routes.Application.index());
	}

	@Security.Authenticated(Secured.class)
	public static Result postBook() {

		DynamicForm form = Form.form().bindFromRequest();
		ArrayList<String> ids = UserInfoDB.getIds();
		String book;
		for (String id : ids) {
			book = form.get(id);
			if (book != null) {
				if (book.contains("Draw")) {
					UserInfoDB.addBook(Secured.getUserInfo(ctx()).getEmail(),
							id, "0");
				} else if (book.contains("Team 1")) {
					UserInfoDB.addBook(Secured.getUserInfo(ctx()).getEmail(),
							id, "1");
				} else if (book.contains("Team 2")) {
					UserInfoDB.addBook(Secured.getUserInfo(ctx()).getEmail(),
							id, "2");
				}
			}
		}

		return redirect(routes.Application.profile());
	}

	/**
	 * Provides the Profile page (only to authenticated users).
	 * 
	 * @return The Profile page.
	 */
	@Security.Authenticated(Secured.class)
	public static Result profile() {
		String query = "SELECT * FROM `pbochene`.`MATCHES` WHERE ID IN ( SELECT MATCHID FROM `pbochene`.`BOOKEDMATCHES` WHERE RESULT = '-1' AND NICK='";
		query += Secured.getUserInfo(ctx()).getEmail();
		query += "' )";
		ArrayList<ArrayList<String>> current = UserInfoDB
				.getCurrentMatches(query);
		query = "SELECT * FROM `pbochene`.`MATCHES` WHERE ID IN ( SELECT MATCHID FROM `pbochene`.`BOOKEDMATCHES` WHERE RESULT = BOOKING AND NICK='";
		query += Secured.getUserInfo(ctx()).getEmail();
		query += "' )";
		ArrayList<ArrayList<String>> won = UserInfoDB.getCurrentMatches(query);
		query = "SELECT * FROM `pbochene`.`MATCHES` WHERE ID IN ( SELECT MATCHID FROM `pbochene`.`BOOKEDMATCHES` WHERE RESULT <> BOOKING AND RESULT <> '-1' AND NICK='";
		query += Secured.getUserInfo(ctx()).getEmail();
		query += "' )";
		ArrayList<ArrayList<String>> lost = UserInfoDB.getCurrentMatches(query);
		return ok(views.html.Profile.render(current, won, lost, "Profile",
				Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx())));
	}

	@Security.Authenticated(Secured.class)
	public static Result bookmatches() {
		String query = "SELECT * FROM `pbochene`.`MATCHES` WHERE STATUS='BEFORE' AND ID NOT IN ( SELECT MATCHID FROM `pbochene`.`BOOKEDMATCHES` WHERE NICK=\"";
		query += Secured.getUserInfo(ctx()).getEmail();
		query += "\" )ORDER BY DATE";
		ArrayList<ArrayList<String>> matches = UserInfoDB
				.getCurrentMatches(query);

		return ok(views.html.Bookmatches.render("Book Matches", matches,
				Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx())));
	}
}
