package controllers;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MatchScraper {

	static Logger logger = Logger.getLogger(MatchScraper.class);

	private enum MATCH_STATUS {
		BEFORE, DURING, AFTER
	}

	private String URL;
	private Calendar rightNow;
	private HashMap<String, String> months;
	private Date url_date;
	private Document doc;
	private Elements content;
	private String[] matches, times, teams1, scores, teams2;
	private MATCH_STATUS[] statuses;
	private DataBase database;
	private ResultSet set;

	void scrap() {
		months = new HashMap<String, String>();
		months.put("Jan", "01");
		months.put("Feb", "02");
		months.put("Mar", "03");
		months.put("Apr", "04");
		months.put("May", "05");
		months.put("Jun", "06");
		months.put("Jul", "07");
		months.put("Aug", "08");
		months.put("Sep", "09");
		months.put("Oct", "10");
		months.put("Nov", "11");
		months.put("Dec", "12");
		rightNow = Calendar.getInstance();
		url_date = rightNow.getTime();
		URL = new String("http://www.livescore.com/soccer/");
		for (int i = 0; i < 4; i++) {
			URL += getDate();
			try {
				scrapMatches();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error("I/O Error", e);
			}
			rightNow.add(Calendar.DAY_OF_YEAR, 1);
			url_date = rightNow.getTime();
			URL = URL.substring(0, URL.lastIndexOf('/') + 1);
		}

	}

	void deleteOutdated() {

		String query = "DELETE FROM `pbochene`.`MATCHES` WHERE STATUS='AFTER'"
				+ " AND ID NOT IN ( SELECT MATCHID FROM `pbochene`.`BOOKEDMATCHES` )";
		database.insertIntoDataBase(query);

	}

	String getDate() {
		String date = url_date.toString();
		date = date.substring(date.indexOf(' ') + 1, date.length());
		String month = date.substring(0, date.indexOf(' '));
		date = date.substring(date.indexOf(' ') + 1, date.length());
		String day = date.substring(0, date.indexOf(' '));
		String year = date.substring(date.lastIndexOf(' ') + 1, date.length());
		month = months.get(month);
		date = year + '-' + month + '-' + day;
		return date;
	}

	void scrapMatches() throws IOException {
		doc = Jsoup.connect(URL).get();
		content = doc.getElementsByClass("even");
		matches = new String[content.size()];
		int i = 0;
		for (Element x : content) {
			matches[i] = x.text();
			i++;
		}
		retrieveMatchInfo();
	}

	void retrieveMatchInfo() {
		times = new String[content.size()];
		teams1 = new String[content.size()];
		scores = new String[content.size()];
		teams2 = new String[content.size()];
		statuses = new MATCH_STATUS[content.size()];
		for (int i = 0; i < content.size(); i += 1) {
			times[i] = matches[i].substring(0, matches[i].indexOf(' '));
			if (times[i].compareTo("FT") == 0) {
				statuses[i] = MATCH_STATUS.AFTER;
			} else if (times[i].contains(":")) {
				statuses[i] = MATCH_STATUS.BEFORE;
			} else {
				statuses[i] = MATCH_STATUS.DURING;
			}
			int firstScore, secondScore;
			String tmp = matches[i].substring(0, matches[i].indexOf(" - "));
			tmp = tmp.substring(tmp.lastIndexOf(' ') + 1, tmp.length());
			firstScore = tmp.length();
			tmp = matches[i].substring(matches[i].indexOf(" - ") + 3,
					matches[i].length());
			tmp = tmp.substring(0, tmp.indexOf(' '));
			secondScore = tmp.length();
			teams1[i] = matches[i].substring(matches[i].indexOf(' ') + 1,
					matches[i].indexOf(" - ") - firstScore - 1);
			scores[i] = matches[i].substring(matches[i].indexOf(" - ")
					- firstScore, matches[i].indexOf(" - ") + 3 + secondScore);
			teams2[i] = matches[i].substring(matches[i].indexOf(" - ") + 4
					+ secondScore, matches[i].length());
		}
		try {
			writeToDataBase();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error("SQL ERROR", e);
		}
	}

	void writeToDataBase() throws SQLException {

		database = new DataBase();
		database.connectToDataBase();

		for (int i = 0; i < content.size(); i++) {
			String query1 = "INSERT INTO `pbochene`.`MATCHES` VALUES (default, \""
					+ statuses[i]
					+ "\", \""
					+ getDate()
					+ "\", \""
					+ times[i]
					+ "\", \""
					+ teams1[i]
					+ "\", \""
					+ scores[i].substring(0, scores[i].indexOf(' '))
					+ "\", \""
					+ scores[i].substring(scores[i].lastIndexOf(' ') + 1,
							scores[i].length())
					+ "\", \""
					+ teams2[i]
					+ "\", \"-1\")";
			String query = "Select * FROM `pbochene`.`MATCHES` WHERE TEAM1 =\"";
			query += teams1[i];
			query += "\" AND TEAM2 = \"";
			query += teams2[i];
			query += "\" AND DATE = \"";
			query += getDate();
			query += "\"";
			set = database.retrieveFromDataBase(query);
			if (set == null) {

				database.insertIntoDataBase(query1);
			} else {
				String[] properties = new String[5];
				properties[0] = statuses[i].toString();
				properties[1] = times[i];
				properties[2] = scores[i].substring(0, scores[i].indexOf(' '));
				properties[3] = scores[i].substring(
						scores[i].lastIndexOf(' ') + 1, scores[i].length());
				if (statuses[i] == MATCH_STATUS.BEFORE
						|| statuses[i] == MATCH_STATUS.DURING) {
					properties[4] = "-1";
				} else if (Integer.parseInt(properties[2]) == Integer
						.parseInt(properties[3])) {
					properties[4] = "0";
				} else if (Integer.parseInt(properties[2]) > Integer
						.parseInt(properties[3])) {
					properties[4] = "1";
				} else {
					properties[4] = "2";
				}
				database.updateMatch(set.getInt(1), properties);

			}

		}
		deleteOutdated();
		database.disconnectFromDataBase();

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MatchScraper scraper = new MatchScraper();
		while (true) {
			scraper.scrap();
			System.out.println("Matches database updated");
			try {
				Thread.sleep(60 * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
