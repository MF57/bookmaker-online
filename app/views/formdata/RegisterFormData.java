package views.formdata;

import java.util.List;

import play.data.validation.ValidationError;

/**
 * Backing class for the login form.
 */
public class RegisterFormData {

	/** The submitted email. */
	public String email = "";
	/** The submitted password. */
	public String password = "";
	public String conpassword = "";
	public String name = "";

	/** Required for form instantiation. */
	public RegisterFormData() {
	}

	/**
	 * Validates Form<LoginFormData>. Called automatically in the controller by
	 * bindFromRequest(). Checks to see that email and password are valid
	 * credentials.
	 * 
	 * @return Null if valid, or a List[ValidationError] if problems found.
	 */
	public List<ValidationError> validate() {

		return null;

	}
}
