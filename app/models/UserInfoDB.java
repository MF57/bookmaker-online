package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import play.db.DB;

/**
 * Provides an in-memory repository for UserInfo.
 */
public class UserInfoDB {

	private static Map<String, UserInfo> userinfos = new HashMap<String, UserInfo>();

	/**
	 * Adds the specified user to the UserInfoDB.
	 * 
	 * @param name
	 *            Their name.
	 * @param email
	 *            Their email.
	 * @param password
	 *            Their password.
	 */
	public static void addUserInfo(String name, String email, String password) {
		String query = "INSERT INTO `pbochene`.`USERS` VALUES (default, \"";
		query += email;
		query += "\", \"";
		query += password;
		query += "\", \"";
		query += name;
		query += "\")";
		Connection connection = DB.getConnection();
		PreparedStatement pst;
		try {
			pst = connection.prepareStatement(query);
			pst.executeUpdate();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		userinfos.put(email, new UserInfo(name, email, password));
	}

	public static void addBook(String email, String id, String bet) {
		String query = "INSERT INTO `pbochene`.`BOOKEDMATCHES` VALUES (\"";
		query += email;
		query += "\", \"";
		query += id;
		query += "\", \"";
		query += bet;
		query += "\", \"-1\")";
		Connection connection = DB.getConnection();
		PreparedStatement pst;
		try {
			pst = connection.prepareStatement(query);
			pst.executeUpdate();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static ArrayList<String> getIds() {
		ArrayList<String> ids = new ArrayList<String>();
		String query = "SELECT ID FROM `pbochene`.`MATCHES` WHERE STATUS='BEFORE'";
		Connection connection = DB.getConnection();
		ResultSet set;
		PreparedStatement pst;
		try {
			pst = connection.prepareStatement(query);
			set = pst.executeQuery();
			while (set.next()) {
				ids.add(set.getString("ID"));
			}
			connection.close();
			return ids;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				connection.close();
				return null;
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return null;
			}
		}

	}

	public static ArrayList<ArrayList<String>> getCurrentMatches(String query) {
		ArrayList<ArrayList<String>> matches = new ArrayList<ArrayList<String>>();

		Connection connection = DB.getConnection();
		ResultSet set;
		PreparedStatement pst;
		try {
			pst = connection.prepareStatement(query);
			set = pst.executeQuery();
			while (set.next()) {
				ArrayList<String> row = new ArrayList<String>();
				if (row.isEmpty()) {
					row.add(set.getString("DATE"));
					row.add(set.getString("TIME"));
					row.add(set.getString("TEAM1"));
					row.add(set.getString("SCORE1"));
					row.add(set.getString("SCORE2"));
					row.add(set.getString("TEAM2"));
					row.add(set.getString("ID"));
					matches.add(row);
				}
			}
			connection.close();
			return matches;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				connection.close();
				return null;
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return null;
			}
		}
	}

	/**
	 * Returns the UserInfo associated with the email, or null if not found.
	 * 
	 * @param email
	 *            The email.
	 * @return The UserInfo.
	 */
	public static UserInfo getUser(String email) {
		UserInfo user = new UserInfo("", "", "");
		String query = "SELECT * FROM `pbochene`.`USERS` WHERE MAIL='";
		query += email;
		query += "'";
		System.out.println(query);
		Connection connection = DB.getConnection();
		ResultSet set;
		PreparedStatement pst;
		try {
			pst = connection.prepareStatement(query);
			set = pst.executeQuery();
			user.setEmail(email);
			if (set.next()) {
				user.setPassword(set.getString("MAIL"));
				user.setName(set.getString("NAME"));
			}
			connection.close();
			return user;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				connection.close();
				return user;
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return user;
			}
		}
	}

	/**
	 * Returns true if email and password are valid credentials.
	 * 
	 * @param email
	 *            The email.
	 * @param password
	 *            The password.
	 * @return True if email is a valid user email and password is valid for
	 *         that email.
	 */
	public static boolean isValid(String email, String password) {
		if (email == null || password == null)
			return false;
		String query = "SELECT * FROM `pbochene`.`USERS` WHERE MAIL='";
		query += email;
		query += "' AND PASSWORD='";
		query += password;
		query += "'";

		Connection connection = DB.getConnection();
		ResultSet set;
		PreparedStatement pst;
		try {
			pst = connection.prepareStatement(query);
			set = pst.executeQuery();
			connection.close();
			if (set.next() == false)
				return false;
			else
				return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				connection.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}

	}

	public static boolean registrationApproved(String query) {

		Connection connection = DB.getConnection();
		ResultSet set;
		PreparedStatement pst;
		try {
			pst = connection.prepareStatement(query);
			set = pst.executeQuery();
			connection.close();
			if (set.next() == true)
				return false;
			else
				return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				connection.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;

		}
	}
}
