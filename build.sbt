name := "play-example-login"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
)  

libraryDependencies ++= Seq(jdbc,anorm,cache,"mysql" % "mysql-connector-java" % "5.1.30")

play.Project.playJavaSettings
